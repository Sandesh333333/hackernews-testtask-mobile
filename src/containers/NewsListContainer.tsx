import React, { useEffect, useState, useCallback } from 'react';
import { useAppSelector, useAppDispatch } from '../store/hooks';

import NewsList from '../components/NewsList';
import api from '../utils/api';
import { sampleFromTopStoriesLoaded } from '../store/actions';
import { selectSampleFromTopStories } from '../store/selectors'; 
import { StoryEntity } from '../types/data';

const NewsListContainer = () => {
  const news: StoryEntity[] = useAppSelector(selectSampleFromTopStories);
  const dispatch = useAppDispatch();

  const [refreshing, setRefreshing] = useState<boolean>(false);

  useEffect(() => {
    setRefreshing(false) 
  }, [news]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    dispatch(
      sampleFromTopStoriesLoaded({ 
        promise: api.Story.getSampleFromTopStories(), 
      })
    );
  }, []);

  useEffect(() => {
    onRefresh();
  }, [])


  return (
    <NewsList
      news={news}
      refreshing={refreshing}
      onRefresh={onRefresh}
    />
  );
};

export default NewsListContainer;
