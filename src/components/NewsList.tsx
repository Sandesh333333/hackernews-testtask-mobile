import React from 'react';
import {
  ScrollView,
  RefreshControl,
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { StoryEntity } from '../types/data';

import NewsCard from './NewsCard';

type NewsListProps = {
  news: StoryEntity[],
  refreshing: boolean,
  onRefresh: () => void
}
const NewsList = ({ news, refreshing, onRefresh }: NewsListProps) => {
  const backgroundStyle = {
    backgroundColor: Colors.lighter
  };

  return (
    <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      style={backgroundStyle}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }
    >
      {news.map((n: StoryEntity) => (
        <NewsCard key={n.id} news={n} />
      ))}
    </ScrollView>
  );
}

export default NewsList;
