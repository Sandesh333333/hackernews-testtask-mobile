import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Linking,
  TouchableHighlight
} from 'react-native';
import { StoryEntity } from '../types/data';

const styles = StyleSheet.create({
  news: {
    padding: 20,
    margin: 10,
    backgroundColor: '#fff',
    borderRadius: 20
  },
  link: {
    color: '#29b5e8'
  },
  title: {
    fontSize: 18,
    marginTop: 7,
    marginBottom: 2
  },
  footer: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  score: {
    fontWeight: 'bold',
  },
  body: {
    marginVertical: 20,
  }
});


type NewsCardProps = {
  news: StoryEntity
}
const NewsCard = ({ news }: NewsCardProps) => {
  return (
    <View style={styles.news}>
      <Text style={styles.title}>{news.title}</Text>
      <TouchableHighlight underlayColor="#61dafb87" onPress={() => Linking.openURL(news.url)}>
        <Text style={styles.link} >{news.url}</Text>
      </TouchableHighlight>
      <View style={styles.body}>
        <Text style={styles.score}>Score: {news.score}</Text>
        <Text>Timestamp: {news.time}</Text>
      </View>
      <View style={styles.footer}>
        <Text>Author: {news.author.id}</Text>
        <Text>Karma: {news.author.karma}</Text>
      </View>
    </View>
  );
}

export default NewsCard;
