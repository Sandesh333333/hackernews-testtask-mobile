import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import NewsListContainer from './containers/NewsListContainer';
import { Provider } from 'react-redux';
import { store } from './store';


const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaView>
        <NewsListContainer />
      </SafeAreaView>
    </Provider>
  );
};


export default App;
