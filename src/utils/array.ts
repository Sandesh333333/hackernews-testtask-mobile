
export function randomSample<T>(data: Array<T>, n: number): Array<T> {
  const indexSet = new Set<number>();
  const last = data.length;
  while (indexSet.size < n && indexSet.size < data.length) {
    indexSet.add(Math.floor(Math.random() * last));
  }

  return Array.from(indexSet).map(index => data[index]);
}
