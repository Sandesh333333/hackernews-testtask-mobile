import { createAction } from 'redux-act';
import { StoryEntity } from '../types/data';

export const sampleFromTopStoriesLoaded =
  createAction<{promise: Promise<StoryEntity[]>, onFinish?: Function}>("GET SAMPLE FROM TOP STORIES");
