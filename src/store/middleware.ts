import { Middleware } from 'redux';
import { RootState } from './index';


const promiseMiddleware: Middleware<{}, RootState> = store => next => action => {
  if (isPromise(action.payload && action.payload.promise)) {
    const onFinish = action.payload && action.payload.onFinish;
    action.payload.promise.then(
      (res: any) => {
        action.payload = res;
        store.dispatch(action);
      },
      (error: any) => {
        action.error = true;
        action.payload = error.response.body;
        store.dispatch(action);
      }
    ).finally(() => onFinish && onFinish());

    return;
  }

  next(action);
};

function isPromise(v: any) {
  return v && typeof v.then === 'function';
};


export { promiseMiddleware };
