import { StoryEntity } from "../types/data";
import { RootState } from './index';

type AppSelector<T> = (state: RootState) => T;
export const selectSampleFromTopStories: AppSelector<StoryEntity[]> = state => state.story.sampleFromTopStories;
