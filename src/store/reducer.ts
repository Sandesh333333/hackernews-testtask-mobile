import { combineReducers } from 'redux';
import story from './reducers/story';

export default combineReducers({
  story
});
